Rivets.js

READ FIRST!!!!!

A buddy recently turned me on to rivets js. It's a sleek little stand alone lib that allows you to leverage data-binding without working with a full framework like Angular. Rivets is a templating system, which in and of itself is not so revolutionary, except unlike handlebars or mustache it has built in two-way binding. This means that, assuming everything is configured properly, when you update your data object the DOM will auto-magically update, and when you update properly associated DOM elements the data model will update. A one and done, one stop shop, configure your page and don't worry about your view and your data falling out of sync again.

This tutorial is going to walk you through building a small mock-shopping app with a product list, a shopping bag and a bill calculator. This app will be for a candy shop.

All of the code for this tutorial can be found in this repository.

Hello Rivets

The most basic thing you can do in rivets is use it's handlebars-like syntax to populate parts of the DOM with data. This is called "text content interpolation", which basically means defining where you want data to appear in the DOM by wrapping it in curly brackets: { somedata }.

Rivets.js is a lightweight data binding and templating system that facilitates building data-driven views. It is agnostic about every aspect of a front-end MV(C|VM|P) stack, making it easy to introduce it into your current workflow or to use it as part of your own custom front-end stack comprised of other libraries.

Usage

<section id="auction">
  <h3>{ auction.product.name }</h3>
  <p>Current bid: { auction.currentBid | money }</p>

  <aside rv-if="auction.timeLeft | lt 120">
    Hurry up! There is { auction.timeLeft | time } left.
  </aside>
</section>
rivets.bind($('#auction'), {auction: auction})
Getting Started and Documentation

Documentation is available on the homepage. Learn by reading the Guide and refer to the Binder Reference to see what binders are available to you out-of-the-box.

Building and Testing

First install any development dependencies.

$ npm install
Building

Rivets.js uses gulp as its build tool. Run the following task to compile + minify the source into dist/.

$ gulp build
Testing

Rivets.js uses mocha as its testing framework, alongside should for expectations and sinon for spies, stubs and mocks. Run the following to run the full test suite.

$ npm test
Contributing

Bug Reporting

Ensure the bug can be reproduced on the latest master.
Open an issue on GitHub and include an isolated JSFiddle demonstration of the bug. The more information you provide, the easier it will be to validate and fix.
Pull Requests

Fork the repository and create a topic branch.
Make sure not to commit any changes under dist/ as they will surely cause conflicts for others later. Files under dist/ are only committed when a new build is released.
Include tests that cover any changes or additions that you've made.
Push your topic branch to your fork and submit a pull request. Include details about the changes as well as a reference to related issue(s).
