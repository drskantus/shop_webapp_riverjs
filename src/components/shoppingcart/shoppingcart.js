'use strict';
// Array where products are stored.
var arrProducts;
// Products information.
var data = {
    title: "Your boat's shopping cart",
    buttonCheckout: {
      name: 'Proceed to Checkout',
      state: true
    },
    productsA: [
      {
        title: 'Towel',
        description: 'Sunny days and warm weather - you clearly need it after a refreshing jump into the cool.',
        price: 10
      },
      {
        title: 'Waterproof camera',
        description: 'Below the sea&rsquo;s surface, a hidden world. Do not miss out to bring those memories back home.',
        price: 49.99
      }
    ],
    productsB: [
      {
        title: 'Enlightening book',
        description: 'There&rsquo;s no rush out there. Take your time for a good read.',
        price: 5.99
      },
      {
        title: 'Pinwheel',
        description: 'Wondering what to do when there is only a light breeze? Back to your childhood!',
        price: 2.50
      }
    ],
    products: [],
    beforeVAT: 0,
    afterVAT: 0,
    VAT: 0
  },
  controller = {
    // Add items to cart.
    onAtbClick: function(e, model) {
      arrProducts = model.data.productsA[model.index];
      if(e.target.id !== 'leftCard') {
        arrProducts = model.data.productsB[model.index];
      }
      var product = arrProducts,
        products = model.data.products,
        i = 0,
        updatePrice = model.controller.updatePrice;
      for(; i < products.length; i++) {
        // Add the number of items.
        if(products[i].title === product.title) {
          products[i].quantity++;
          updatePrice(model.data);
          return;
        }
      }
      // Add item to the array.
      products.push(product);
      products[products.length - 1].quantity = 1;
      // Update the price of the item.
      updatePrice(model.data);
    },
    // Add the number of items.
    addItem: function(e, model) {
      model.data.products[model.index].quantity++;
      model.controller.updatePrice(model.data);
    },
    // Remove the number of items.
    removeItem: function(e, model) {
      var index = model.index,
        products = model.data.products,
        product = products[index],
        updatePrice = model.controller.updatePrice;
        // Validate to avoid remove all the items.
        if(product.quantity > 1) {
          product.quantity--;
          updatePrice(model.data);
          return;
        }
        products.splice(index, 1);
        updatePrice(model.data);
    },
    // Remove event from the cart.
    removeAllItem: function(e, model) {
      var index = model.index,
        products = model.data.products,
        updatePrice = model.controller.updatePrice;
      products.splice(index, 1);
      updatePrice(model.data);
    },
    // Update the price, calculate beforeVAT, afterVAT and VAT.
    updatePrice: function(data) {
      var products = data.products,
        product,
        beforeVAT = 0,
        i = 0;
      for(; i < products.length; i++) {
        product = products[i];
        beforeVAT += product.price * product.quantity;
      }
      data.beforeVAT = beforeVAT;
      data.afterVAT = beforeVAT * 0.20;
      data.VAT = beforeVAT + data.afterVAT;
    },
    sendOrder: function() {
      data.buttonCheckout.name = 'Sending your order...';
      data.buttonCheckout.state = false;
    }
  };

// Formatters for the prices.
rivets.formatters.price = function(val) {
  var spl = String(val).split('.'),
    dollarsArray = spl[0].split(''),
    lastDollar = dollarsArray.length - 1,
    pow,
    i;
  if(dollarsArray.length > 3) {
    dollarsArray.reverse();
    for(i = lastDollar; i > -1; i--) {
      if(i % 3 === 0 && i !== 0) {
        dollarsArray.splice(i, 0, ',');
      }
    }
    spl[0] = dollarsArray.reverse().join('');
  }
  if(spl.length > 1) {
    spl[1] = spl[1].substr(0, 2);
    if(spl[1].length < 2) {
      spl[1] += '0';
    }
  }else {
    spl[1] = '00';
  }
  return '<abbr title="USD">$</abbr>' + spl.join('.');
};

// Formatters for quantity of values.
rivets.formatters.length = function(val) {
  return val.length;
}

// Bind to id(section-shop).
rivets.bind(document.querySelector('#section-shop'), {
  data: data,
  controller: controller
});
